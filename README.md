# php-json-store

## Docker

Start an instance of the json store with docker-compose.
```bash
docker-compose up -d
```

## API

### Create a store

```text
GET /:store_name?action=create_store
```


### Remove a store

```text
GET /:store_name?action=remove_store
```


### Read from store

```text
GET /:store_name
GET /:store_name/:json_key
GET /:store_name/:json_key/:json_key/...
```


### Set to store

```text
GET /:store_name/:json_key?action=set&value=foo&type=string
```


### Remove from store

```text
GET /:store_name/:json_key?action=remove
```

### Append to an array

```text
GET /:store_name/:json_key?action=append&value=bar
```


### Parameters

| Attribute | Type | Description |
| ---- | --- | --- |
| `action` | string | Action to be performed |
| `value` | string | Value that will be set on key |
| `type` | string | Specific data type that the value will be converted to |
| `store_name` | string | Name of the store |
| `json_key` | string | The name of a key in this store |


### Available data types

| Data type | Description |
| --- | --- |
| `bool` | Values like 'false' and '0' will be converted to bool false. All other values will be converted to bool true. |
| `int` | The given value will be converted to an integer value with the php intval() function. See [php documentation](https://php.net/manual/en/function.intval.php) for further information. |
| `float` | The given value will be converted to a float value with the php floatval() function. See [php documentation](https://php.net/manual/en/function.floatval.php) for further information. |
| `double` | The given value will be converted to a float value with the php floatval() function. See [php documentation](https://php.net/manual/en/function.floatval.php) for further information.  |
| `string` | The given value will be converted to a string value with the php strval() function. See [php documentation](https://php.net/manual/en/function.strval.php) for further information. |
| `json` |The given value will be converted to a json value with the php json_decode() function. See [php documentation](https://php.net/manual/en/function.json-decode.php) for further information.  |


### Features

Features are placeholders in value parameter, that the store is able to detect and are able to replace.

| Feature name | Description |
| --- | --- |
| `:uuid` | If the value `:uuid` is detected, the store replace this placeholder by an uuid |
| `:now` | If the value `:now` is detected, the store replace this placeholder by an timestamp with the current datetime. The timestamp has the structure `Y-m-d H:i:s` |