<?php

class JsonDB
{
    protected $storeName;
    protected $pathToStorage;
    protected $store;
    
    public function __construct($pathToStorage, $storeName)
    {
        $this->storeName = $storeName;
        $this->pathToStorage = $pathToStorage;
    }

    public function storeExists()
    {
        return file_exists($this->getPathToStore());   
    }

    public function createStore()
    {
        $this->save();
    }

    public function deleteStore()
    {
        unlink($this->getPathToStore());
    }

    public function save()
    {   
        file_put_contents($this->getPathToStore(), serialize($this->store));
    }

    public function load()
    {
        $this->store = unserialize(file_get_contents($this->getPathToStore()));
    }

    public function get($key, $default = null)
    {
        return $key !== null 
                ? array_get($this->store, $key, $default)
                : $this->store;
    }

    public function set($key, $value)
    {
        if (! is_array($this->store)) {
            $this->store = (array) $this->store; 
        }
        array_set($this->store, $key, $value);
    }

    public function add($key, $value)
    {
        $arr = $this->get($key);
        if (!is_array($arr)) {
            $arr = (array) $arr;
        }
        $arr[] = $value;
        $this->set($key, $arr);
    }

    public function remove($key)
    {
        array_unset($this->store, $key);
    }

    public function has($key)
    {
        return array_has($this->store, $key);
    }

    public function getName()
    {
        return $this->storeName;    
    }

    protected function getPathToStore()
    {
        return $this->pathToStorage.'/'.$this->storeName;
    }
}
