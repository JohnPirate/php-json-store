<?php

if (!function_exists('root_path')) {
    
    function root_path()
    {
        return realpath(__DIR__.'/../..');
    }
}

if (!function_exists('json_response')) {

    function json_response($data, $statuscode = 200)
    {
        header('Content-Type: application/json', true, $statuscode);
        echo json_encode($data);
        exit;
    }
}

if (!function_exists('uuidv4')) {
    /**
     * Return a v4 uuid
     *
     * @see https://stackoverflow.com/a/2040279
     *
     * @return string
     */
    function uuidv4()
    {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
}

if (!function_exists('feature_process')) {

    function feature_process($data)
    {
        if (is_string($data)) {
            return replace_placeholders($data);
        }
        // TODO(ssandriesser): process arrays
        return $data;
    }
}

if (!function_exists('replace_placeholders')) {

    function replace_placeholders($data)
    {
        $data = str_replace(':uuid', uuidv4(), $data);
        $data = str_replace(':now', date('Y-m-d H:i:s'), $data);
        return $data;
    }
}

if (!function_exists('convert_type')) {
    
    function convert_type($data, $type, $nullable = true)
    {
        if ($nullable && ($data === null || $data === 'null')) {
            return null;    
        }
        switch ($type) {
            case 'bool':
                return in_array($data, array('false', 0)) ? false : !!$data;
                break;
            case 'int': 
                return intval($data);
                break;
            case 'float':
            case 'double':
                return floatval($data);
                break;
            case 'json':
                return json_decode($data, true);
                break;
            default:
                return strval($data);
        }
    }
}

if (! function_exists('http_post_request_data')) {
    /**
     * Returns the post request data
     *
     * @param null|string $key
     * @param null|mixed  $optional
     *
     * @return string|array|null
     */
    function http_post_request_data($key = null, $optional = null)
    {
        if (null === $key) {
            return $_POST;
        }
        return isset($_POST[$key]) ? $_POST[$key] : $optional;
    }
}

if (! function_exists('http_get_request_data')) {
    /**
     * Returns the get request data
     *
     * @param null|string $key
     * @param null|mixed  $optional
     *
     * @return string|array|null
     */
    function http_get_request_data($key = null, $optional = null)
    {
        if (null === $key) {
            return $_GET;
        }
        return isset($_GET[$key]) ? $_GET[$key] : $optional;
    }
}

if (! function_exists('http_request_data')) {
    /**
     * Returns the get or post request data.
     *
     * @param null|string $key
     * @param null|mixed  $optional
     *
     * @return string|array|null
     */
    function http_request_data($key = null, $optional = null)
    {
        return http_post_request_data($key)
            ? http_post_request_data($key)
            : http_get_request_data($key, $optional);
    }
}
