<?php
/*
|---------------------------------------------------------------------------
| Array Helper Functions
|---------------------------------------------------------------------------
*/
if (! function_exists('array_flatten')) {
    /**
     * Flatten a multi-dimensional array into a single level.
     *
     * @param array $array
     * @param int   $depth
     *
     * @return array
     */
    function array_flatten($array, $depth = INF)
    {
        $result = array();
        foreach ($array as $item) {
            if (! is_array($item)) {
                $result[] = $item;
            } elseif ($depth === 1) {
                $result = array_merge($result, array_values($item));
            } else {
                $result = array_merge($result, array_flatten($item, $depth - 1));
            }
        }
        return $result;
    }
}

if (! function_exists('dot_array')) {

    /**
     * Flatten a multi-dimensional associative array with dots
     *
     * @param array  $array
     * @param string $prepend
     *
     * @return array
     */
    function dot_array(array $array, $prepend = '')
    {
        $results = array();
        foreach ($array as $key => $value) {
            if (is_array($value) && ! empty($value)) {
                $results = array_merge($results, dot_array($value, $prepend.$key.'.'));
            } else {
                $results[$prepend.$key] = $value;
            }
        }
        return $results;
    }
}

if (! function_exists('array_get')) {
    /**
     * Get a value from an array using "dot" notation.
     *
     * @param array      $array
     * @param string|int $key
     * @param null|mixed $default
     *
     * @return mixed|null
     */
    function array_get($array, $key, $default = null)
    {
        if (! is_array($array)) {
            return $default;
        }
        if (is_null($key)) {
            return $array;
        }
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }
        if (strpos($key, '.') === false) {
            return isset($array[$key]) ? $array[$key] : $default;
        }
        foreach (explode('.', $key) as $segment) {
            if (is_array($array) && array_key_exists($segment, $array)) {
                $array = $array[$segment];
            } else {
                return $default;
            }
        }
        return $array;
    }
}

if (! function_exists('array_set')) {
    /**
     * Set a value to an array using "dot" notation.
     *
     * @param array      $array
     * @param string|int $key
     * @param mixed      $value
     *
     * @return array|mixed
     */
    function array_set(array &$array, $key, $value)
    {
        if (is_null($key)) {
            return $array = $value;
        }
        $keys = explode('.', $key);
        while (count($keys) > 1) {
            $key = array_shift($keys);
            if (! isset($array[$key]) || ! is_array($array[$key])) {
                $array[$key] = array();
            }
            $array = &$array[$key];
        }
        $array[array_shift($keys)] = $value;
        return $array;
    }
}

if (! function_exists('array_unset')) {
    /**
     * Unset a value from an array using "dot" notation.
     *
     * @param array      $array
     * @param string|int $key
     *
     * @return void
     */
    function array_unset(array &$array, $key)
    {
        if (is_null($key)) {
            $array = null;
            return;
        }
        $keys = explode('.', $key);
        $keyUnset = array_pop($keys);
        while (count($keys) > 0) {
            $key = array_shift($keys);
            if (! isset($array[$key])) {
                return;
            }
            $array = &$array[$key];
        }

        if (is_array($array) && is_numeric($keyUnset)) {
            array_splice($array, $keyUnset, 1);
        } else {
            unset($array[$keyUnset]);
        }
    }
}

if (! function_exists('array_has')) {
    /**
     * Check if an item or items exist in an array using "dot" notation.
     *
     * @param array        $array
     * @param string|array $keys
     *
     * @return bool
     */
    function array_has(array $array, $keys)
    {
        if (is_null($keys)) {
            return false;
        }
        $keys = (array) $keys;
        if (! $array) {
            return false;
        }
        if ($keys === array()) {
            return false;
        }
        foreach ($keys as $key) {
            $subKeyArray = $array;
            if (array_key_exists($key, $array)) {
                continue;
            }
            foreach (explode('.', $key) as $segment) {
                if (is_array($subKeyArray) && array_key_exists($segment, $subKeyArray)) {
                    $subKeyArray = $subKeyArray[$segment];
                } else {
                    return false;
                }
            }
        }
        return true;
    }
}

if (! function_exists('array_dot_normalize')) {
    /**
     * Normalize an array to use "dot" notation.
     *
     * @param array $array
     *
     * @return array
     */
    function array_dot_normalize(array $array)
    {
        $array_buffer = dot_array($array);
        $array = array();
        foreach ($array_buffer as $key => $val) {
            array_set($array, $key, $val);
        }
        return $array;
    }
}
