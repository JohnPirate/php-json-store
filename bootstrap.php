<?php

error_reporting(E_ALL);
// error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', true);
ini_set('html_errors', true);


// CORS Control
$allowedHosts = array(
    '*',
);
$allowedMethods = array(
    'OPTIONS',
    'GET',
);
$allowedHeaders = array(
    'X-Requested-With',
);
header('Access-Control-Allow-Origin: '.implode(', ', $allowedHosts));
header('Access-Control-Allow-Methods: '.implode(', ', $allowedMethods));
header('Access-Control-Allow-Headers: '.implode(', ', $allowedHeaders));

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    exit;
}

require_once __DIR__.'/src/Support/helper_functions.php';
require_once __DIR__.'/src/Support/array_helper_functions.php';
require_once __DIR__.'/src/JsonDB.php';

try {

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    throw new \RuntimeException('Allowed request methods are only GET and OPTIONS');
}

$requestUriOffset = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');

if (! $requestUriOffset) {
    json_response(array(
        'title' => 'json storage',
        'description' => trim("
# API

## Create a store

GET /:store_name?action=create_store


## Remove a store

GET /:store_name?action=remove_store


## Read from store

GET /:store_name
GET /:store_name/:json_key
GET /:store_name/:json_key/:json_key/...


## Set to store

GET /:store_name/:json_key?action=set&value=foo&type=string


## Unset from store

GET /:store_name/:json_key?action=unset
        "),
        "data_types" => array(
            'bool', 'int', 'float','double', 'string', 'json'
        )
    ));
    throw new \RuntimeException("No Storename found.");
}

$action = http_get_request_data('action');

$requestUriOffsetArr = explode('/', $requestUriOffset, 2);
$storeName = $requestUriOffsetArr[0];
$jsonOffset = isset($requestUriOffsetArr[1]) ? str_replace('/', '.', $requestUriOffsetArr[1]) : null;

$pathToJsonStorage = root_path().'/storage';
$jsonDb = new \JsonDB($pathToJsonStorage, $storeName);


if ($action === 'create_store') {
    if ($jsonDb->storeExists()) {
        throw new \RuntimeException("Store {$storeName} already exist.");
    }
    $jsonDb->createStore();
    json_response(array(
        'message' => "Create {$storeName} store successful.",
    ));
}
if (! $jsonDb->storeExists()) {
    throw new \RuntimeException("Store {$storeName} not found.");
}
if ($action === 'remove_store') {
    $jsonDb->deleteStore();
    json_response(array(
        'message' => "Remove {$storeName} store successful.",
    ));
}
$jsonDb->load();

$val = http_get_request_data('value');
$type = http_get_request_data('type');

$val = convert_type($val, $type);
$val = feature_process($val);


if ($action === 'set') {
    $jsonDb->set($jsonOffset, $val);
    $jsonDb->save();
} elseif ($action === 'remove') {
    if (!$jsonDb->has($jsonOffset)) {
        throw new \RuntimeException("Nothing to remove on {$jsonOffset}.");
    }

    $jsonDb->remove($jsonOffset);
    $jsonDb->save();
    json_response(array(
        'message' => "Remove data from {$jsonOffset} successful.",
    ));
} elseif ($action === 'append') {
    $jsonDb->add($jsonOffset, $val);
    $jsonDb->save();
}
json_response($jsonDb->get($jsonOffset));

} catch (\RuntimeException $ex) {
    json_response(array(
        'message' => $ex->getMessage(),
    ), 400);    
    exit;
    
} catch (\Exception $ex) {
    json_response(array(
        'message' => $ex->getMessage(),
    ), 500);    
    exit;
}
